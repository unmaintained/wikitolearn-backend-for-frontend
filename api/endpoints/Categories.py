from flask import jsonify
from flask.views import MethodView

import requests
import urllib.parse

from ..response import response_ok, response_error


categories = {
    "Informatica": {
        "parent": None,
        "name": "Informatica",
        "displayName": "Informatica",
        "subcategories": [
            {
                "name": "Programmazione",
                "displayName": "Programmazione",
                "courses": [
                        {"displayName": "Java", "name": "Java"},
                        {"displayName": "Python", "name": "Python"}
                ]
            },
            {
                "name": "Informatica_Teorica",
                "displayName": "Informatica Teorica",
                "courses": [
                        {"displayName": "Fondamenti di informatica", "name": "Fondamenti di informatica"},
                        {"displayName": "Algoritmica", "name": "Algoritmica"}
                ]
            }
        ],
        "courses": [ {"displayName": "Reti IP", "name": "Reti IP"} ]
    }
}


class Categories(MethodView):

    def get(self, category_name: str):
        """if name is provided, return the details for the category, otherwise returns all the root categories"""
        if category_name is None:
            # list root categories
            return response_ok([category for key, category in categories.items() if category["parent"] == None])
        else:
            category = categories[category_name]
            return response_ok(category)
            # retrive details for submitted category
