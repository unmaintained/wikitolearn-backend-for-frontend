from flask import jsonify
from flask.views import MethodView

import requests
import urllib.parse

from ..response import response_ok, response_error


class Courses(MethodView):

    def get(self, course_name: str):
        """Return the page with the passed title"""
        course_name_clean = urllib.parse.quote(course_name, safe="")
        r = requests.get(
            f"http://it.wikitolearn.org/api.php?action=coursetree&coursetitle=Course:{course_name_clean}&format=json")
        response = r.json()
        if len(response["coursetree"]["response"]["levelsTwo"]) != 0:
            course = response["coursetree"]["response"]
            course["name"] = course_name
            return response_ok(course)
        else:
            return response_error(f"The course {course_name} was not found on the server.", 404)
