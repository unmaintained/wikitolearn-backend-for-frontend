from flask import jsonify, request
from flask.views import MethodView

import requests
import urllib.parse

from ..cache import cache
from ..response import response_ok, response_error

from functools import wraps


pages_cache = {}


def cache_on_revision_id(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        title = kwargs["title"]
        if title in pages_cache:
            safe_title = urllib.parse.quote(title, safe="")
            r = requests.get(
                f"https://restbase.wikitolearn.org/it.wikitolearn.org/v1/page/title/{safe_title}")

            if r.status_code == 404:
                return response_error(f"Page {title} not found on server", 404)
            if r.status_code != 200:
                return response_error(f"Unknown error in page info fetching", response.status_code)

            response = r.json()
            page_details = response["items"][0]
            revision_id = page_details["rev"]
            if pages_cache[title]["revisionId"] == revision_id:
                return response_ok(pages_cache[title])
        return func(*args, **kwargs)

    return decorated_function


class Pages(MethodView):
    def __init__(self):
        self.latest_revisions = {}

    def should_invalidate_cache():
        return False

    @cache_on_revision_id  # return from cache if the revision id is unchanged
    def get(self, title: str):
        """Return the page with the passed title"""
        safe_title = urllib.parse.quote(title, safe="")
        r = requests.get(
            f"https://restbase.wikitolearn.org/it.wikitolearn.org/v1/page/title/{safe_title}")

        if r.status_code == 404:
            return response_error(f"Page {title} not found on server", 404)
        if r.status_code != 200:
            return response_error(f"Unknown error in page info fetching", r.status_code)

        response = r.json()

        page_details = response["items"][0]
        revision_id = page_details["rev"]

        r = requests.get(
            f"https://it.wikitolearn.org/api.php?action=parse&oldid={revision_id}&origin=*&format=json")
        if r.status_code is not 200:
            return response_error(f"Unknown error in revision fetching", r.status_code)

        response = r.json()["parse"]

        display_title = response["displaytitle"]
        content = response["text"]["*"]

        page = {
            "displayTitle": display_title,
            "title": title,
            "content": content,
            "revisionId": revision_id
        }

        pages_cache[title] = page
        return response_ok(page)
