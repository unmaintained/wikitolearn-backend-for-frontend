from flask import jsonify, make_response


def response_ok(data, headers = {}):
    return response({
        "error": None,
        "data": data
    }, 200, headers)


def response_error(message, code, headers = {}):
    return response({
        "error": message,
    }, code, headers)

def response(data, code = 200, headers = {}):
    data["status"] = code
    resp = make_response(
        jsonify(data),
        code
    )
    for header, value in headers.items():
        resp.headers[header] = value
    return resp
